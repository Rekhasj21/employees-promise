const fs = require('fs')
let content

function employeeId(id, dataObj) {
    return new Promise((resolve, reject) => {
        const employeeIdList = dataObj.filter(employee => id.includes(employee.id))
        try {
            content = JSON.stringify(employeeIdList)
        }
        catch (err) {
            console.log(err.message)
        }
        fs.writeFile("jsonFile/employee1.json", content, (err, response) => {
            if (err) {
                reject(err);
            } else {
                resolve("Success");
            }
        });
    })
}
function groupDataOnCompanieName(dataObj) {
    return new Promise((resolve, reject) => {
        const groupDataOnCompanies = dataObj.reduce((acc, dataList) => {
            if (acc[dataList.company]) {
                acc[dataList.company] = dataList
            } else {
                acc[dataList.company] = []
            }
            return acc
        }, {})
        try {
            content = JSON.stringify(groupDataOnCompanies)
        }
        catch (err) {
            console.log(err.message)
        }
        fs.writeFile('jsonFile/employee2.json', content, (err, response) => {
            if (err) {
                reject(err);
            } else {
                resolve("Success");
            }
        });
    })
}
function dataOfCompanyPowerpuff(dataObj) {
    return new Promise((resolve, reject) => {
        const dataForCompany = dataObj.filter(({ company }) => company === "Powerpuff Brigade")
        try {
            content = JSON.stringify(dataForCompany)
        }
        catch (err) {
            console.log(err.message)
        }
        fs.writeFile('jsonFile/employee3.json', content, (err, response) => {
            if (err) {
                reject(err);
            } else {
                resolve("Success");
            }
        });
    })
}

function dataWithoutParticularId(dataObj) {
    return new Promise((resolve, reject) => {
        const dataWithoutId2 = dataObj.filter(({ id }) => id !== 2)
        try {
            content = JSON.stringify(dataWithoutId2)
        }
        catch (err) {
            console.log(err.message)
        }
        fs.writeFile('jsonFile/employee4.json', content, (err, response) => {
            if (err) {
                reject(err);
            } else {
                resolve("Success");
            }
        });
    })
}

function sortData(dataObj) {
    return new Promise((resolve, reject) => {
        const dataForSort = dataObj
        const sortDataBasedOnCompanyNameOrId = dataForSort.sort((a, b) => {
            const aCompany = a.company.trim()
            const bCompany = b.company.trim()
            if (aCompany < bCompany) {
                return -1
            }
            if (aCompany > bCompany) {
                return 1
            }
            if (a.id < b.id) {
                return -1
            }
            if (a.id > b.id) {
                return 1
            }
            return 0
        })
        try {
            content = JSON.stringify(sortDataBasedOnCompanyNameOrId)
        }
        catch (err) {
            console.log(err.message)
        }
        fs.writeFile('jsonFile/employee5.json', content, (err, response) => {
            if (err) {
                reject(err);
            } else {
                resolve("Success");
            }
        });
    });
}

function swapData(dataObj) {
    return new Promise((resolve, reject) => {
        const swapdata = dataObj
        const index93 = swapdata.findIndex(record => record.id === 93);
        const index92 = swapdata.findIndex(record => record.id === 92);
        [swapdata[index92], swapdata[index93]] = [swapdata[index93], swapdata[index92]];
        try {
            content = JSON.stringify(swapdata)
        }
        catch (err) {
            console.log(err.message)
        }
        fs.writeFile('jsonFile/employee6.json', content, (err, response) => {
            if (err) {
                reject(err);
            } else {
                resolve("Success");
            }
        });
    });
}
function evenAndBirthdayDate(dataObj) {
    return new Promise((resolve, reject) => {
        const dataToFindEvenAndAddBrthday = dataObj
        let today = new Date();
        const currentDate = today.getFullYear() + "-" + today.getMonth() + 1 + "-" + today.getDate();
        const updatedDataAfterAddingBirthDayDate = dataToFindEvenAndAddBrthday.map(employee => {
            if (employee.id % 2 === 0) {
                return { ...employee, birthday: currentDate }
            }
            else {
                return employee
            }
        })
        try {
            content = JSON.stringify(updatedDataAfterAddingBirthDayDate)
        }
        catch (err) {
            console.log(err.message)
        }
        fs.writeFile('jsonFile/employee7.json', content, (err, response) => {
            if (err) {
                reject(err);
            } else {
                resolve("Success");
            }
        });
    });
}

module.exports = { employeeId, swapData, dataOfCompanyPowerpuff, dataWithoutParticularId, sortData, evenAndBirthdayDate, groupDataOnCompanieName }